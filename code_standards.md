## GoLang Code Standards

- Organize code into packages
- code patterns and conventions
- writing clear and understandable code
- Unwritten Go conventions


Packages: 
There are two key areas of code organization in Go that will make a huge impact on the 
usability, testability, and functionality of your code:

Code Organization: 
- Package Naming
- Package Organization

Package Organization - Libraries
- Packages contain code that have a single purpose
	EX. archive, cmd, crypto, errors
	- The package name makes it clear as to it's purpose is

- When a group of packages provides a common set of functionalities with different
  implimentations, they're organized under a parent. 
  	EX. Enoding Package => ascii85, asn1, base64, binary

Commanalities -> 
	- packages names describe their purpose
	- It's very easy to see what a packge does by looking at the name
	- Names are generally short
	- When necessary, use a descriptive parent package, and sereral childern implementing
	  the functionality -- like the encoding package

Package Organization - Applications
	- When you have an application, the package organization is subtly different. The difference is the command, the executable that ties all of those packages together.

	- Application package organization has a huge impact on the testability and functionality of your system.

	- When writing an application your goal should be to write code that is easy to understand, easy to refactor, and easy for someone else to maintain.

	- Most libraries focus on providing a singularly scoped function; logging, encoding, network access. The application will tie all of those libraries together to create a tool or service. That tool or service will be much larger in scope.

When you're building an application, you should organize your code into packages, but those packages should be centered on two categories:
	1) Domain Types: types that model your business functionality and objects
	2) Services: packages that operate on or with the domain types
[link to article on Domain (v) Services](https://medium.com/@benbjohnson/standard-package-layout-7cdbc8391fc1)

Domain Types: is the substance of your application. If you have an inventory application, your domain types might include Product and Supplier. If you have an HR administration system, your domain types might include Employee, Department, and Business Unit.

The package containing your domain types should also define the interfaces between your domain types and the rest of the world. These interfaces define the things you want to do with your domain types:
	1) ProductService
	2) SupplierService
	3) AuthenticationService
	4) EmployeeStorage
	5) RoleStorage

Your domain type package should be the root of your application repository. This makes it clear to anyone opening the codebase what types are being used, and what operations will be performed on those types. The domain type package should NOT have any external dependencies. Communicating only two things the "what," and "how."

It exists for the sole purpose of describing your types and their behaviors.

The implementations of your domain interfaces should be in separate packages, organized by dependency.

Dependencies include:
	1) External data sources
	2) Transport logic (http, RPC)

You should have one package per dependency. 

Why one package per dependency?
	- Simple Testing
	- Easy substitution/replacement
	- No circular dependencies

#### Naming Conventions

	"there are two hard things in computer science: cache invalidation, naming things, and off-by-one errors"

A package name should have the following characteristics:
	- short
		- Prefer "transport" over "transportmechanisms"
	- clear
		- Name for clarity based on function: "bytes"
		- Name to describe implementation of external dependency: "postgres"

Packages should provide functionality for one and only one purpose. Avoid catchall packages:
	** DO NOT USE **
	- util
	- helpers
	- etc
Frequently they're a sign that you're missing an interface somewhere.

Example: `util.ConvertOtherToThing() should be refactored into a "Thinger" interface.

catchall packages are always the first place you'll run into problems with testing and circular dependencies. 

#### Variables

Some common conventions for variable names:
	- use camelCase not snake_case
	- use single letter variables to represent indexes
		- `for i := 0; i < 10; i++ {}`
		- Use `i` first `j` second `k` for the third
	- use short but descriptive variable names for other things
		- `var count int`
		- `var cust Customer // don't use c for Customer`
	- use repeated letters to represent a collection/slice/array
		- `var tt [](star)Thing`
		- inside a loop/range, use the single letter
			- `for i, t := range tt {}`
		- This makes is more clear, repeated letters = slice or map
	- Avoid a package-level function name that repeats the package name
		- GOOD: `log.Info()`
		- BAD: `log.LogInfo()`
		- The package name already declares the purpose of the package, so there's no need to repeat it
	- Go code doesn't have setters and getters: this isn't Java don't write it like Java
		- GOOD: `custSvc.Customer()`
		- BAD: `custSvc.GetCustomer()`
	- If your interface has only one function, append "-er" to the function name:
		- `type Stringer interface { String() string }`

There are no bonus points in Go for obfuscating your code by using unnecessarily short variables. Use the scope of the variable as your guide. You can determine the length of the optimal variable name based on length from the declaration. The farther away from declaration you use it, the longer the name should be.


#### Interfaces

	- If your interface has more than one function, use a name to represent its functionality

	EX. 
	` type CustomerStorage interface {
		Customer(id int) (*Cusomter, error) 
		Save(c *Cusomter) error
		Delete(id int) error
	}`

	- You could of used "CustomerSaver," or "CustomerDatabaser," logically it's a CustomerStorage interface. You can break out of the convention if it follows a logical break. It's okay to not use the (er) everywhere. Choose readability over anything. 

	- Inside a package separate code into logical concerns
	- If the package deals with multiple types, keep the logic for each type in its own source file
	- In the package that defines your domain objects, define the types and interfaces for each object in the same source file

#### Tips

	- Make comments in full sentences, always.

	- Use goimports to manage your imports, and they'll always be in canonical order. Standard lib first, external next

	- Avoid the else clause. Especially in error handling

