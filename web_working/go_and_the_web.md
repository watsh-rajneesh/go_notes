#### GO and the Web ____________________________________________________________________

	- Concepts in Web Principles 
		1) Request: request data from users, (POST, GET, Cookie and URL)
		2) Response: response data from server to clients
		3) Conn: connection between clients and servers
		4) Handler: request handling logic and response generation

	- When creating endpoints there are three main questions you should ask. 
		1) How do we listen to a port? 
		2) How do we accept client requests?
		3) How do we allocate handlers? 

	- Go uses <ListenAndServe> to handle these steps: initalize a server object, call 
	  <net.Listen("tcp", addr)> to setup a TCP listener and listen to a specific
	  address and port. 

***Get into HTTP Package***

	- The core of this section is going to be about <Conn> and <ServeMux> 

	- Goroutine in Conn: Go uses goroutines for every job initiated by Conn in order to
	  achieve high concurrency and preformance, so every job is independent. 

	EX.1 Code that waits for new connections from clients

		c, err := srv.newConn(rw)
		if err != nil {
			continue
		}
		go c.serve() 

	EX.1-A
		- This creates a new goroutine for every connetion and passes the handler that
		  is able to read data from the request to the goroutine. 

	- Customiezd ServeMux: 
	
	EX.2 The structure of defautl router: 
		
		type ServeMux struct {
			mu sync.RWMutex		// because of concurrency, we have to use a mutex here
			m map[string]muxEntry	// router rules, every string mapping to a handler
		}

	EX.3 The struct of MuxEntry
		
		type muxEntry struct {
			explicit bool 	// exact match or not
			h 	Handler
		}

	EX.4 The interface of Handler 

		type Handler interface {
			ServeHTTP(ResponseWriter, *Request) 	// routing implementer
		}

	EX.4-A
		- <Handler> is an interface, but if the function didn't implement this interface
		  then how is this impliment? -> The answer lies in another type called
		  <HandlerFunc> in the <http> package. 

	EX.5 HandlerFunc Implimenting Handler

		type HanderFunc func(ResponseWritter, *Request)

		// ServeHTTP calls f(w,r)
		func (f HandlerFunc) ServeHTTP(w ResponseWritter, r *Request) {
			f(w, r) 
		}

	EX.5-A
		- The router calls <mux.handler.ServeHTTP(w, r)> when it recieves requests. In
		  other words, it calls the <ServeHTTP> interface of the handlers which have
		  implemented it. 

	EX.6 mux.handler how it works

		func (mux *ServeMux) handler(r *Request) Handler {
			mux.my.RLock()
			defer mux.my.RUnlock()

			// host-specific pattern takes precedence over generic ones
			h := mux.match(r.Host + r.URL.Path)
			if h == nil {
				h = mux.match(r.URL.Path)
			}
			if h == nil {
				h = NotFoundHandler()
			}
			return h
		}

	EX.6-A
		- The router uses the requests URL as a key to find the corresponding handler
		  saved in the map, then calls handler.ServeHTTP to execute functions to 
		  handle the data. 

***Custom Handerls for ListenAndServe Function***

	- Go supports customized routers. The second argument of <ListenAndServe> is for
	  configuring customized routers. Its an interface of <Handler>. Therefore, any
	  router that implements the <Handler> interface can be used. 

	EX.7 Implement a Simple Router

		package main 

		import (
			"fmt"
			"net/http"
		)

		type MyMux struct {
		}

		func (p *MyMux) ServeHTTP(w http.ResponseWritter, r *http.Request) {
			if r.URL.Path == "/" {
				sayhelloName(w, r)
				return
			}
			http.NotFound(w, r)
			return
		}

		func sayhelloName(w http.ResponseWritter, r *http.Request) {
			fmt.Fprintf(w, "Hello Myroute!")
		}

		func main() {
			mux := &MyMux{}
			http.ListenAndeServe(":9090", mux)
		}

***ROUTING*** 

	- If you do not want to use a Router, you can still achieve what we wrote in the 
	  above section by replacing the second arguments to <ListenAndServe> to <nil>
	  and registering the URLs using a <HandleFunc> function which goes through all
	  the registered URLs to find the best match, so care must be taken about the 
	  order of the registering. 

	EX.8 Routing without Router

		http.HandleFunc("/", views.ShowAllTasksFunc)
		http.HandleFunc("/complete/", views.CompleteTaskFunc)
		http.HandleFunc("/delete/", views.DeleteTaskFunc)

		// ShowAllTasksFunc is used to handle the "/" URL which is the default ons
		// TODO add http404 error
		func ShowAllTasksFunc(w http.ResponseWritter, r *http.Request) {
			if r.Method == "GET" {
				context := db.GetTasks("Pending") // true when you want non deleted tasks
				// db is a package which iteracts with the database
				if message != "" {
					context.Message = message
				}
				homeTemplate.Execute(w, context)
				message = ""
			} else {
				message = "Method not allowed"
				http.Redirect(w, r, "/", http.StatusFound)
			}
		}

	EX.8-A
		- This is fine for simple applications which don't requre parameterized routing. 
	
	
	- When the match is made on the <HandleFunc> function, the URL is matched, so
		  suppose we are writing a todo list manager, and we want to delte a task so 
		  the URL we decide for that app is </delete/1>, so we register the delete URL
		  like this <http.HandleFunc("/delete/", views.DeleteTaskFunc)> </delete/1/>, 
		  this URL matches closest with the "/delete/" URL than any other URL so the 
		  <r.URL.path> we get the entire URL of the request. 

	EX.9 Delete Routing Example 

		http.HandleFunc("/delete/", views.DeleteTaskFunc)
		// deleteTaskFunc is used to delete a task, trash = move to recycle bin, 
		// delete = permanent delete
		func DeleteTaskFunc(w http.ResponseWritter, r *http.Request) {
			if r.Method == "DELETE" {
				id := r.URL.Path[len("/delete/"):]
				if id == "all" {
					db.DeleteAll()
					http.Redirect(w, r, "/", http.StatusFound)
				} else {
					err = db.DeleteTask(id)
					if err != nil {
						message = "Error deleting task"
					} else {
						message = "Task Deleted"
					}
					http.Redirect(w, r, "/", http.StatusFound)
				}
			} else {
				message = "Method not allowed"
				http.Redirect(w, r, "/", http.StatusFound)
			}
		}

	EX.9-A 
		- In the function which handles </delete/> URL we take its complete URL, which 
		  is </delete/1>, then we take a slice of the string and extract everything
		  which stats after the delte word which is the actual parameter, in this case
		  it is <1>. Then we use <strconv> package to convert it to an integer and 
		  delete the task with that taskID. 
		- In more complex scenarios too we can use the method, the advantage is that we
		  don't have to use any third part toolkit, but then again third party toolkits
		  are useful in their own right, you need to make a decision which method 
		  you'd prefer. No answer is the right answer. 

***Code Execution Flow***

	1) Call <http.HandleFunc>
		- call handlefunc of DefaultServeMux
		- Call HandleFunc of defaultservemux
		- Add router routes to map[string]muxEntry of DefaultServeMux
	2) Call <http.ListenAndServe(":9090", nil)
		- Instantiate Server
		- Call ListenAndServe method of Server
		- Call net.Listen("tcp", addr) to listen to port 
		- Start a looop and accept requests in the loop body
		- Instantiate a Conn and start a goroutine for every request: <go c.serve()> 
		- Read request data: <w, err := c.readRequest()> 
		- Check whether handler is empty or not, if it's emplty then use DefaultServeMux
		- Call ServeHTTP of handler
		- Choose handler by URL and execute code in that handler function: 
		  <mux.handler.ServeHTTP(w, r)> 
		- How to choose handler: 
			A. Check router rules for this URL
			B. Call ServeHTTP in taht handler if there is one. 
			C. Call ServeHTTP of NotFoundHandler otherwise




































