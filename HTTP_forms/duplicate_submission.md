#### Duplicate Submissions ________________________________________________________________

	- The solution to duplicate submissions is to add a hidden field with a unique token to 
	  your form, and to always check this token before processing the incoming data. Also, 
	  if you are using AJAX to submit a form, use JavaScript to disable the submit button
	  once the form has been submitted. 

	EX.1 Using MD5 Hash (time stamp) 

	func login(w http.ResponseWritter, r *http.Request) {
		fmt.Println("method: ", r.Method) // get request method
		if r.Method == "GET" {
			crutime := time.Now().Unix()
			h := md5.New()
			io.WriteString(h, strconv.FormatInt(crutime, 10))
			token := fmt.Sprintf("%x", h.Sum(nil))

			t, _ := template.ParseFiles("login.gtpl")
			t.Execute(w, token)
		} else {
			// log in request
			r.ParseForm()
			token := r.Form.Get("token")
			if token != "" {
				// check token validity
			} else {
				// give error if no token
			}

			fmt.Println("username length:", len(r.Form["username"][0]))
			// print information server side
			fmt.Println("username:", template.HTMLEscapeString(r.Form.Get("username")))
			fmt.Println("password:", template.HTMLEscapeString(r.Form.Get("password")))
			template.HTMLEscap(w, []byte(r.Form.Get("username"))) // respond to client
		}
	}











